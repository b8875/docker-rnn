FROM ubuntu:16.04

RUN apt-get update && apt-get install -y \
    findutils \
    file \
    libunwind8 \
    libunwind-dev \
    pkg-config \
    build-essential \
    cmake \
    gcc-multilib \
    g++-multilib \
    git \
    m4 \
    scons \
    zlib1g \
    zlib1g-dev \
    libprotobuf-dev \
    protobuf-compiler \
    libprotoc-dev \
    libgoogle-perftools-dev \
    python-dev \
    python \
    python-yaml \
    wget \
    libpci3 \
    libelf1 \
    libelf-dev \
    libpci-dev \
    vim \
    cmake \
    cmake-qt-gui \
    libboost-program-options-dev \
    gfortran \
    openssl \
    libssl-dev \
    libboost-filesystem-dev \
    libboost-system-dev \
    libboost-dev \
    libgflags-dev \
    ocl-icd-opencl-dev \
    libgoogle-glog-dev

ARG rocm_ver=1.6.2

# Get files needed for gem5, and apply patches
RUN git clone --single-branch --branch agutierr/master-gcn3-staging https://gem5.googlesource.com/amd/gem5 && chmod 777 /gem5

RUN git clone -b roc-1.6.x https://github.com/RadeonOpenCompute/ROCT-Thunk-Interface.git
RUN git clone --single-branch https://github.com/RadeonOpenCompute/ROCR-Runtime.git
RUN git clone --recursive -b roc-1.6.x https://github.com/RadeonOpenCompute/hcc.git
RUN git clone --single-branch https://github.com/ROCm-Developer-Tools/HIP/
RUN git clone --single-branch https://github.com/ROCmSoftwarePlatform/MIOpenGEMM/
RUN git clone --single-branch https://b8875@bitbucket.org/b8875/miopen-rnn.git
RUN git clone --single-branch https://github.com/RadeonOpenCompute/rocm-cmake/


# Get and apply patches to various repos
COPY patch /patch
COPY rocBLAS /rocBLAS

RUN git -C /gem5/ checkout d0945dc2 && git -C /gem5/ apply /patch/gem5.patch
RUN git -C /ROCR-Runtime/ checkout 08039b && git -C /ROCR-Runtime/ apply /patch/hsa.patch
RUN git -C /hcc/ checkout e8cf396 
RUN git -C /hcc/clang checkout 540c9ca
RUN git -C /hcc/compiler checkout 363dd5c
RUN git -C /hcc/compiler-rt checkout fb18901
RUN git -C /hcc/lld checkout 0cb1996
RUN git -C /hcc/rocdl checkout dc669c5
RUN git -C /hcc/ apply /patch/hcc.patch 
RUN git -C /HIP/ checkout 0e3d824e && git -C /HIP/ apply /patch/hip.patch
RUN git -C /MIOpenGEMM/ checkout 9547fb9e

ENV ROCM_PATH /opt/rocm
ENV HCC_HOME ${ROCM_PATH}/hcc
ENV HSA_PATH ${ROCM_PATH}/hsa
ENV HIP_PATH ${ROCM_PATH}/hip
ENV HIP_PLATFORM hcc
ENV PATH ${ROCM_PATH}/bin:${HCC_HOME}/bin:${HSA_PATH}/bin:${HIP_PATH}/bin:${PATH}
ENV LD_LIBRARY_PATH ${ROCM_PATH}/rocblas/lib:${LD_LIBRARY_PATH}

# Create build dirs for machine learning ROCm installs
RUN mkdir -p /ROCT-Thunk-Interface/build && \
    mkdir -p /ROCR-Runtime/src/build && \
    mkdir -p /hcc/build && \
    mkdir -p /HIP/build && \
    mkdir -p /rocBLAS/build && \
    mkdir -p /rocm-cmake/build && \
    mkdir -p /MIOpenGEMM/build && \
    mkdir -p /miopen-rnn/build

# Do the builds
WORKDIR /ROCT-Thunk-Interface/build
RUN cmake \
    -DCMAKE_INSTALL_PREFIX=/opt/rocm/libhsakmt .. && \
    make && make install

WORKDIR /ROCR-Runtime/src/build
RUN cmake \
    -DCMAKE_INSTALL_PREFIX=/opt/rocm/hsa \
    -DCMAKE_PREFIX_PATH="/opt/rocm/libhsakmt" .. && \
    make -j$(nproc) && make install
WORKDIR /opt/rocm/hsa/lib
RUN ln -s /opt/rocm/libhsakmt/lib/libhsakmt.so libhsakmt.so.1 && \
    ln -s /opt/rocm/hsa/lib/libhsa-runtime64.so libhsa-runtime64.so.1

WORKDIR /hcc/build
RUN cmake \
    -DCMAKE_INSTALL_PREFIX=/opt/rocm/hcc \
    -DCMAKE_PREFIX_PATH="/opt/rocm/hsa;/opt/rocm/libhsakmt" .. && \
    make -j$(nproc) && make install

WORKDIR /rocm-cmake/build
RUN cmake .. && cmake --build . --target install

WORKDIR /HIP/build
RUN cmake \
    -DHIP_PLATFORM=hcc \
    -DCMAKE_INSTALL_PREFIX=/opt/rocm/hip \
    -DHCC_HOME=/opt/rocm/hcc \ 
    -DCMAKE_PREFIX_PATH="/opt/rocm/hsa;/opt/rocm/libhsakmt;/opt/rocm/hcc/rocdl/lib" \
    -DCMAKE_BUILD_TYPE=Release \
    -DHSA_PATH=/opt/rocm/hsa .. && \
    make -j$(nproc) && make install

WORKDIR /rocBLAS/build
RUN CXX=/opt/rocm/hcc/bin/hcc cmake \
    -DCMAKE_INSTALL_PREFIX=/opt/rocm/rocblas \
    -DTensile_LOGIC=hip_lite \
    -DHSA_LIBRARY=/opt/rocm/hsa/lib/libhsa-runtime64.so \
    -DHSA_HEADER=/opt/rocm/hsa/include/hsa \
    -Dhip_DIR=/opt/rocm/hip \
    -Dhcc_DIR=/opt/rocm/hcc \
    -DROCM_DIR=/opt/rocm/share/rocm/cmake .. && \ 
    make -j$(nproc) && make install

# Install default ROCm programs
RUN wget -qO- repo.radeon.com/rocm/archive/apt_${rocm_ver}.tar.bz2 \
    | tar -xjv \
    && cd apt_${rocm_ver}/pool/main/ \
    && dpkg -i r/rocm-utils/* 

WORKDIR /MIOpenGEMM/build
RUN cmake \
    -DCMAKE_INSTALL_PREFIX=/opt/rocm/miopengemm \
    -DOPENCL_LIBRARIES="/usr/lib/x86_64-linux-gnu/libOpenCL.so" .. && \
    make && make install

RUN mkdir -p /.cache/miopen && chmod 777 /.cache/miopen

WORKDIR /miopen-rnn/build
RUN CXX=/opt/rocm/hcc/bin/hcc cmake \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/opt/rocm \
    -DMIOPEN_BACKEND=HIP \
    -Dmiopengemm_DIR=/opt/rocm/miopengemm/lib/cmake/miopengemm \
    -Dhip_DIR=/opt/rocm/hip \
    -DMIOPEN_MAKE_BOOST_PUBLIC=ON \
    -DCMAKE_PREFIX_PATH="/opt/rocm/hip;/opt/rocm/hcc;/opt/rocm/rocdl;/opt/rocm/miopengemm;/opt/rocm/hsa;/opt/rocm/rocblas" \
    -DMIOPEN_CACHE_DIR=/opt/rocm/.cache/miopen \
    -DMIOPEN_AMDGCN_ASSEMBLER_PATH=/opt/rocm/opencl/bin \
    -DCMAKE_CXX_FLAGS="-isystem /usr/include/x86_64-linux-gnu -D__STRICT_ANSI__" .. && \
    make -j$(nproc) && make install

WORKDIR /gem5
RUN scons -j$(nproc) build/GCN3_X86/gem5.opt --ignore-style

WORKDIR /

RUN mkdir /tmp2 && chmod 777 /tmp2
COPY tests/ tests/
COPY .cache/ /opt/rocm/.cache/
CMD bash
