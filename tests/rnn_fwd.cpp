#include <hip/hip_runtime_api.h>
#include <miopen/miopen.h>

#include <cstdio>
#include <ctime>
#include <iostream>
#include <numeric>
#include <vector>

struct Tensor {
    void *data;
    size_t data_size;
    Tensor(std::vector<int>dims)
    {
        data_size = std::accumulate(dims.begin(),
                    dims.end(), 1, std::multiplies<int>());
        std::vector<float> host_data(data_size);
        std::srand(std::time(0));
        for (int i = 0; i < data_size; i++) host_data[i] = std::rand();

        data = new float[data_size];
        memcpy(data, host_data.data(), data_size*sizeof(float));
    }
    ~Tensor()
    {
      free(data);
    }
};

struct Tensor2 {
   void *data;
   size_t data_size;
   Tensor2(std::vector<int>dims)
   {
      data_size = std::accumulate(dims.begin(),
                                  dims.end(), 1, std::multiplies<int>());
      std::vector<float> host_data(data_size);
      for (int i = 0; i < data_size; i++) host_data[i] = 0;
      data = new float[data_size];
      memcpy(data, host_data.data(), data_size*sizeof(float));
   }
   ~Tensor2()
   {
     free(data);
   }
};


struct TensorNdArray {
    miopenTensorDescriptor_t *desc;

    TensorNdArray(std::vector<int>dim,
                  std::vector<int>stride,
                  int num)
    {
        miopenDataType_t type = miopenFloat;
        desc = new miopenTensorDescriptor_t[num];
        for (int i = 0; i < num; i++)
        {
            miopenCreateTensorDescriptor(&desc[i]);
            miopenSetTensorDescriptor(desc[i],
                                      type, dim.size(),
                                      &dim[0], &stride[0]);
        }

    }
    ~TensorNdArray()
    {
      delete[] desc;
    }
};


int main(int argc, char *argv[])
{

    miopenHandle_t miopen_handle;
    miopenCreate(&miopen_handle);
	
	int hidden_size = 128;
	int batch_size = 1;
	int time_steps = 4;
	std::string rnn_type = "lstm";
	miopenRNNMode_t rnn_mode;

    if (rnn_type == "vanilla")
       rnn_mode = miopenRNNRELU;
    else if (rnn_type == "gru")
       rnn_mode = miopenGRU;
    else if (rnn_type == "lstm")
       rnn_mode = miopenLSTM;
    else
       exit(-1);
    size_t weight_size_ = 0;
    size_t workspace_size_ = 0;

    miopenRNNInputMode_t input_mode = miopenRNNskip;
    miopenRNNDirectionMode_t direction = miopenRNNunidirection;
    miopenDataType_t type = miopenFloat;
    miopenRNNBiasMode_t biasMode = miopenRNNwithBias;
    miopenRNNAlgo_t algo = miopenRNNdefault;
    miopenStatus_t status;

    miopenRNNDescriptor_t *rnn_desc = new miopenRNNDescriptor_t;
    miopenCreateRNNDescriptor(rnn_desc);
    

    miopenSetRNNDescriptor(*rnn_desc,
                           hidden_size,
                           1,
                           input_mode,
                           direction,
                           rnn_mode,
                           biasMode,
                           algo,
                           type);
    

    // forward parameters
    TensorNdArray xDescArray({batch_size, hidden_size},
                             {hidden_size, 1}, time_steps);

    TensorNdArray yDescArray({batch_size, hidden_size},
                             {hidden_size, 1}, time_steps);
    TensorNdArray hx_desc({1, batch_size, hidden_size},
                          {hidden_size * batch_size, hidden_size, 1}, 1);
    TensorNdArray hy_desc({1, batch_size, hidden_size},
                          {hidden_size * batch_size, hidden_size, 1}, 1);
    TensorNdArray cx_desc({1, batch_size, hidden_size},
                          {hidden_size * batch_size, hidden_size, 1}, 1);
    TensorNdArray cy_desc({1, batch_size, hidden_size},
                          {hidden_size * batch_size, hidden_size, 1}, 1);

    std::cout << "Hidden Size:" << hidden_size << std::endl;

    // forward data
    Tensor x(std::vector<int>{hidden_size, batch_size * time_steps});
    Tensor y(std::vector<int>{hidden_size, batch_size * time_steps});
    Tensor hx(std::vector<int>{hidden_size, batch_size});
    Tensor hy(std::vector<int>{hidden_size, batch_size});
    Tensor cx(std::vector<int>{hidden_size, batch_size});
    Tensor cy(std::vector<int>{hidden_size, batch_size});

    status = miopenGetRNNParamsSize(miopen_handle,
                                    rnn_desc[0],
                                    xDescArray.desc[0],
                                    &weight_size_,
                                    type);

    if (status != 0) {
        std::cout << "RNN ParamsSize Failure!" << std::endl;
        exit(-1);
    }

    // RNN weight
    Tensor weights_(std::vector<int>
                    {static_cast<int>(weight_size_ / sizeof(float)), 1});

    TensorNdArray wDesc({static_cast<int>
          (weights_.data_size), 1, 1}, {1, 1, 1}, 1);
    std::cout << "RNN weight" << std::endl;

    // RNN workspace
    status = miopenGetRNNWorkspaceSize(miopen_handle,
                              rnn_desc[0],
                              time_steps,
                              xDescArray.desc,
                              &workspace_size_);
    
    std::cout << "RNN workspace" << std::endl;
    if (status != 0) {
        std::cout << "RNN WorkspaceSize Failure!" << std::endl;
        exit(-1);
    }

    Tensor2 workspace_(std::vector<int>
                {static_cast<int>(workspace_size_ / sizeof(float)), 1});

 
    std::cout << "RNN forwarding" << std::endl;
    // RNN forwarding
    status = miopenRNNForwardInference(miopen_handle,
                                        rnn_desc[0],
                                        time_steps,
                                        xDescArray.desc,
                                        x.data,
                                        hx_desc.desc[0],
                                        hx.data,
                                        cx_desc.desc[0],
                                        cx.data,
                                        wDesc.desc[0],
                                        weights_.data,
                                        yDescArray.desc,
                                        y.data,
                                        hy_desc.desc[0],
                                        hy.data,
                                        cy_desc.desc[0],
                                        cy.data,
                                        workspace_.data,
                                        workspace_size_
    );

    return 0;

}
